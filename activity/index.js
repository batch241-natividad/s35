
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://admin123:admin123@cluster0.svvw27f.mongodb.net/s35?retryWrites=true&w=majority", {
	
	useNewUrlParser : true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json());

app.use(express.urlencoded({extended: true}));


const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  const { username, password } = req.body;

  const newUser = new User({
    username,
    password
  });

  newUser.save((saveErr, savedUser) =>{
  	if(saveErr){
  	return res.status(400).send("error: error message");
  	} else {
  		return res.status(201).send("New user registered")
  	}
  })
});

app.listen(port, () => console.log(`Server running at port ${port}`));


